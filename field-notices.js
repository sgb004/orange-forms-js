/**
 * @name: FieldNotices
 * @author: sgb004
 * @version: 0.1.0
 */

import { FFNotices } from './ff-notices';
export class FieldNotices extends FFNotices {
	static __notices = {
		empty: 'Please, complete this field.',
		pattern: 'The value is not in a correct format.',
		minlength: 'The value has a length less than {{ minlength }}.',
		maxlength: 'The value has a length greater than {{ maxlength }}.',
	};
	static externalNotifications = 'orange-forms-field-notices';
}
