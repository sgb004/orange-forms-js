import './css/styles.css';
import './components/ff-notices-cpt';
import { FieldCpt } from './components/field-cpt';
import { OrangeForm } from './orange-form';

globalThis['orangeForms'] = {};
globalThis['FieldCpt'] = FieldCpt;
document.addEventListener('DOMContentLoaded', () => {
	const forms = document.querySelectorAll('.orange-form');
	for (let iterator of forms) {
		globalThis['orangeForms'][
			iterator.getAttribute('name')
		] = new OrangeForm(iterator);
	}
});
