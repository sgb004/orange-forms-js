/**
 * @name: FormNotices
 * @author: sgb004
 * @version: 0.1.0
 */

import { FFNotices } from './ff-notices';

export class FormNotices extends FFNotices {
	__notices = {
		notFound:
			'The data could not be sent, because the destination url was not found.',
		notAvailable:
			'The data could not be sent, because the destination url is not available.',
		error: 'An error occurred while trying to send the data.',
		checkFields: 'Please, check notices in fields.',
		fieldsNotExists: 'The field does not exist.',
		extraField:
			'The response returned an error in a field that does not exist.',
	};
	static externalNotifications = 'orange-forms-form-notices';
}
