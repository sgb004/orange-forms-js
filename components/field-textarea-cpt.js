/**
 * @name: FieldTextareaCp
 * @author: sgb004
 * @version: 0.1.0
 */

import { FieldCpt } from './field-cpt';

export class FieldTextareaCpt extends FieldCpt {
	constructor() {
		super(document.createElement('textarea'));
		if (!this.value) {
			this.field.innerText = this.innerHTML;
		}
	}
}

customElements.define('field-textarea-cpt', FieldTextareaCpt);
