# Orange Forms JS - Components

## Requirements

These components need lit-element

## Component field-cpt

This component is a input with a label and the component ff-notices-cpt added. This adds an attribute field to OrangeForm can process it.

## Usage

In your main .js add field-cpt.js, if previously you added orange-forms.js you do not need to add these file.

```javascript
import 'field-cpt';
```

Add the tag in your html:

```html
<field-cpt
	type="field_type"
	name="field_name"
	id="field_id"
	value="field_value"
	autocomplete="off"
	textlabel="label_text"
	required
></field-cpt>
```

or:

<!-- prettier-ignore-start -->
```html
<field-cpt type="field_type" name="field_name" id="field_id" value="field_value" autocomplete="off" textlabel="label_text" required></field-cpt>
```
<!-- prettier-ignore-end -->

### Attributes

The following attributes are similar to those used in an input: id, name, type, value, placeholder, required, pattern, minlength, maxlength and autocomplete, the next attributes are used by this component.

#### textlabel

Text used in label, if the field is required, a pseudo-element after will be added, you can edit it using css.

```css
.orange-form field-cpt[required] label:after {
	content: ' *';
}
```

#### hidelabel

Hide label, if placeholder is empty textlabel will use in placeholder. When it is true a css class hide-label will be added in the component field-cpt.

#### notices

The file [field-notices.js](./../field-notices.js) has the messages used in the component, as 'Please, complete this field.', if you want to translate or change these texts you can use a script tag:

```html
<script id="orange-forms-field-notices" type="application/json">
	{ "empty": "This field can not be empty." }
</script>
```

Individually, the texts can be changed directly in the component using the notices attribute, for example:

```html
<field-cpt
	type="text"
	name="field_name"
	id="field_id"
	notices='{"empty": "This field can not be empty."}'
	textlabel="Name"
	required
></field-cpt>
```

or:

<!-- prettier-ignore-start -->

```html
<field-cpt type="text" name="field_name" id="field_id" notices='{"empty": "This field can not be empty."}' textlabel="Name" required></field-cpt>
```
<!-- prettier-ignore-end -->

Or you can change directly using javascript:

```javascript
document.getElementById('lead_form_add_name').notices.empty =
	'This field can not be empty.';
```

### Properties

#### fieldNotices

Return ff-notices-cpt component.

### Methods

#### isEmpty

Return true if the field is empty or false if it is not.

#### isPatternValid

This function checks that the value of the field matches the pattern attribute, returns true if it is correct or false if it is not.

#### isValid

Return true if the field is valid or false if it is not.

#### addNotice(notices, type)

Add a notice to component ff-notices-cpt in the field.

-   **notices** is an array with notices to display, it allow send a simple string.

-   **type** is a type of notice or notices, it will appear in the notice in the class attribute.

## Component field-textarea-cpt

This component is a textarea with a label and the component ff-notices-cpt added.

Add the tag in your html:

```html
<field-textarea-cpt
	type="field_type"
	name="field_name"
	id="field_id"
	autocomplete="off"
	textlabel="label_text"
	required
>
	My text
</field-textarea-cpt>
```

or:

<!-- prettier-ignore-start -->
```html
<field-textarea-cpt type="field_type" name="field_name" id="field_id" autocomplete="off" textlabel="label_text" required>My text</field-textarea-cpt>
```
<!-- prettier-ignore-end -->

You can use value attribute:

```html
<field-textarea-cpt
	type="field_type"
	name="field_name"
	id="field_id"
	value="My text"
	autocomplete="off"
	textlabel="label_text"
	required
></field-textarea-cpt>
```

or:

<!-- prettier-ignore-start -->
```html
<field-textarea-cpt type="field_type" name="field_name" id="field_id" value="My text" autocomplete="off" textlabel="label_text" required></field-textarea-cpt>
```
<!-- prettier-ignore-end -->

The attributes, properties, methods and functionality work similar to field-cpt.

## Component ff-notices-cpt

This component display notices of a field or form.

### Methods

#### add(notices, type)

Add notices to component

-   **notices** is an array with notices to display, it allow send a simple string.

-   **type** is a type of notice or notices, it will appear in the notice in the class attribute.

#### reset

Clear notices of the field or form.
