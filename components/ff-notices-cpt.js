/**
 * @name: FFNoticesCpt
 * @author: sgb004
 * @version: 0.1.0
 */

import { LitElement, html } from 'lit-element';

class FFNoticesCpt extends LitElement {
	#__list;

	createRenderRoot() {
		return this;
	}

	static get properties() {
		return {};
	}

	constructor() {
		super();
		this.#__list = document.createElement('ul');
	}

	add(notices, type = '') {
		let item;
		if (typeof notices == 'string') {
			notices = { notices };
		}
		for (let key in notices) {
			item = document.createElement('li');
			item.classList.add(type);
			item.innerHTML = notices[key];
			this.#__list.appendChild(item);
		}
	}

	reset() {
		this.#__list.innerHTML = '';
	}

	render() {
		return html`${this.#__list}`;
	}
}

customElements.define('ff-notices-cpt', FFNoticesCpt);
