/**
 * @name: FieldCpt
 * @author: sgb004
 * @version: 0.1.1
 */

import { LitElement, html, css } from 'lit-element';
import { FieldNotices } from './../field-notices';
export class FieldCpt extends LitElement {
	#__fieldNotices;
	isField = true;

	createRenderRoot() {
		return this;
	}

	static get properties() {
		return {
			id: { type: String },
			name: { type: String },
			type: { type: String },
			value: { type: String },
			placeholder: { type: String },
			required: { type: String },
			pattern: { type: String },
			minlength: { type: Number },
			maxlength: { type: Number },
			autocomplete: { type: String },
			hidelabel: { type: Boolean },
			textlabel: { type: String },
		};
	}

	constructor(field, label) {
		super();
		this.field = field ?? document.createElement('input');
		FieldNotices.init();
		this.notices = FieldNotices.merge(this.field.getAttribute('notices'));

		this.field.addEventListener('input', this.keypress.bind(this));
		this.label = label ?? document.createElement('label');

		if (this.getAttribute('type') && this.field.getAttribute('type')) {
			this.type = 'text';
		}
	}

	static get styles() {
		return css`
			:host {
				display: block;
			}
		`;
	}

	render() {
		this.setAttribute('field', 'field');
		return html`${this.label}${this.field}<ff-notices-cpt />`;
	}

	set id(id) {
		this.field.id = id + '-field';
		this.label.setAttribute('for', this.field.id);
	}

	get name() {
		return this.field.name;
	}

	set name(name) {
		this.field.setAttribute('name', name);
	}

	set type(type) {
		this.field.setAttribute('type', type);
	}

	set value(value) {
		this.field.value = value;
	}

	get value() {
		return this.field.value;
	}

	set placeholder(placeholder) {
		this.field.setAttribute('placeholder', placeholder);
	}

	set required(required) {
		if (required || required == '') {
			this.field.setAttribute('required', required);
		} else {
			this.field.removeAttribute('required');
		}
	}

	get required() {
		let required = this.field.getAttribute('required');
		return required === null ? false : true;
	}

	get pattern() {
		return this.field.getAttribute('pattern');
	}

	set pattern(pattern) {
		this.field.setAttribute('pattern', pattern);
	}

	get minlength() {
		return parseInt(this.field.getAttribute('minlength'));
	}

	set minlength(minlength) {
		this.setAttribute('minlength', minlength);
	}

	get maxlength() {
		return parseInt(this.field.getAttribute('maxlength'));
	}

	set maxlength(maxlength) {
		this.setAttribute('maxlength', maxlength);
	}

	set autocomplete(autocomplete) {
		this.field.setAttribute('autocomplete', autocomplete);
	}

	set hidelabel(hidelabel) {
		const placeholder = this.getAttribute('placeholder');
		if (hidelabel) {
			this.classList.add('hide-label');
			if (!placeholder) {
				this.field.setAttribute(
					'placeholder',
					this.getAttribute('textlabel')
				);
			}
		} else {
			this.classList.remove('hide-label');
			this.field.removeAttribute('placeholder');
		}
	}

	set textlabel(textlabel) {
		this.label.innerHTML = textlabel;
	}

	isEmpty() {
		let value = this.value.trim();
		return value === '';
	}

	isPatternValid() {
		let pattern = this.pattern;
		pattern = new RegExp(pattern);
		return pattern.test(this.value);
	}

	isValid() {
		let isValid = true;
		if (this.isEmpty() && this.required) {
			this.addNotice(this.notices.empty, 'danger');
			isValid = false;
		} else {
			if (this.pattern != null) {
				if (!this.isPatternValid()) {
					this.addNotice(this.notices.pattern, 'danger');
					isValid = false;
				}
			}

			if (!isNaN(this.minlength)) {
				if (this.value.length < this.minlength) {
					this.addNotice(
						this.notices.minlength.replace(
							'{{ minlength }}',
							this.minlength
						),
						'danger'
					);
					isValid = false;
				}
			}

			if (!isNaN(this.maxlength)) {
				if (this.value.length > this.maxlength) {
					this.addNotice(
						this.notices.maxlength.replace(
							'{{ maxlength }}',
							this.maxlength
						),
						'danger'
					);
					isValid = false;
				}
			}
		}
		return isValid;
	}

	keypress(e) {
		this.fieldNotices.reset();
		this.isValid();
	}

	get fieldNotices() {
		if (!this.#__fieldNotices) {
			let fieldNotices = this.field.parentNode.querySelector(
				'ff-notices-cpt'
			);
			if (!fieldNotices) {
				fieldNotices = document.createElement('ff-notices-cpt');
				this.field.parentNode.appendChild(fieldNotices);
			}
			this.#__fieldNotices = fieldNotices;
		}
		return this.#__fieldNotices;
	}

	addNotice(notice, type = '') {
		this.fieldNotices.add(notice, type);
	}
}

customElements.define('field-cpt', FieldCpt);
