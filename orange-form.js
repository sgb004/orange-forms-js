/**
 * @name: OrangeForm
 * @author: sgb004
 * @version: 0.1.0
 */

import { FormNotices } from './form-notices';
import { CustomFilters } from 'custom-filters-js';

export class OrangeForm {
	#__customFilters;
	#__isSubmit = false;
	#__fields;
	#__formNotices;

	constructor(form) {
		this.form = form;
		this.addEventListener('submit', this.submit);

		this.form.setAttribute('novalidate', 'novalidate');

		this.#__customFilters = new CustomFilters(this);
		FormNotices.init();
		this.notices = FormNotices.merge(form.getAttribute('notices'));

		this.#__formNotices = this.form.querySelector('ff-notices-cpt.to-form');

		if (this.form.getAttribute('reset') === null) {
			this.reset = true;
		}
	}

	get fields() {
		if (this.#__isSubmit && this.#__fields) {
			return this.#__fields;
		}
		let fieldsElements = this.form.querySelectorAll('*[field]');
		let fields = {};
		let field, name;
		for (field of fieldsElements) {
			name = field.name;
			if (!field.isField) {
				field = new globalThis['FieldCpt'](field);
			}
			fields[name] = field;
		}
		fields = this.dispatchFilter('fields', fields);
		if (this.#__isSubmit) {
			this.#__fields = fields;
		}
		return fields;
	}

	isValid() {
		let isValid = true;
		const fields = this.fields;
		for (let field in fields) {
			if (!fields[field].isValid()) {
				isValid = false;
			}
		}
		return this.dispatchFilter('isValid', isValid, fields);
	}

	fieldsNoticesReset() {
		const fields = this.fields;
		for (let field in fields) {
			fields[field].fieldNotices.reset();
		}
	}

	get data() {
		let data = {};
		const fields = this.fields;
		for (let field in fields) {
			data[fields[field].name] = fields[field].value;
		}
		return this.dispatchFilter('data', data, fields);
	}

	get redirectSuccess() {
		return this.form.getAttribute('redirect-success');
	}

	get redirectError() {
		return this.form.getAttribute('redirect-error');
	}

	get reset() {
		return this.form.getAttribute('reset') === 'true';
	}

	set reset(reset) {
		this.form.setAttribute('reset', reset);
	}

	send() {
		const data = this.data;
		let opts = this.dispatchFilter(
			'sendOpts',
			{
				method: 'put',
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
				},
			},
			data
		);
		opts['body'] = JSON.stringify(data);
		return new Promise((resolve, reject) => {
			fetch(this.form.action, opts)
				.then((response) => {
					if (response.status == 200) {
						return response.json();
					} else if (response.status == 404) {
						reject({ notice: this.notices.notFound });
					} else {
						reject({ notice: this.notices.notAvailable });
					}
				})
				.then((data) => {
					resolve(data);
				})
				.catch((error) => {
					reject({ notice: this.notices.error, error });
				});
		});
	}

	submit(e) {
		e.preventDefault();
		this.#__isSubmit = true;
		this.#__formNotices.reset();
		this.fieldsNoticesReset();
		this.form.classList.add('sending');
		this.dispatchEvent('beforeValid');
		if (this.isValid()) {
			this.dispatchEvent('beforeSend');
			this.send()
				.then((data) => {
					let fieldsHasNotices = false;
					const fields = this.fields;

					this.dispatchEvent('afterSend', { data });

					for (let field in data.fields) {
						fieldsHasNotices = true;
						if (fields[field]) {
							fields[field].addNotice(data.fields[field], 'danger');
						} else {
							throw {
								notice: this.notices.extraField,
								error:
									this.notices.fieldsNotExists +
									'\n\tFIELD: ' +
									field +
									'\n\tNOTICE: ' +
									data.fields[field],
							};
						}
					}

					if (fieldsHasNotices) {
						this.#__formNotices.add(this.notices.checkFields, 'danger');
					} else if (data.success) {
						let redirect = this.redirectSuccess;

						if (this.reset) {
							this.form.reset();
						}

						if (redirect) {
							location.href = redirect;
						} else {
							this.#__formNotices.add(data.notice, 'success');
						}
					} else {
						let redirect = this.redirectError;
						if (redirect) {
							location.href = redirect;
						} else {
							this.#__formNotices.add(data.notice, 'danger');
						}
					}
					this.#__isSubmit = false;
					this.form.classList.remove('sending');
				})
				.catch((error) => {
					this.dispatchEvent('afterSendError');
					if (!error.notice) {
						error = { notice: this.notices.error, error };
					}
					this.#__formNotices.add(error.notice, 'danger');
					if (error.error) {
						console.log(error.error);
					}
					this.#__isSubmit = false;
					this.form.classList.remove('sending');
				});
		} else {
			this.#__formNotices.add(this.notices.checkFields, 'danger');
		}
	}

	dispatchEvent(event, detail) {
		let e = new CustomEvent(event, { detail });
		this.form.dispatchEvent(e);
	}

	addEventListener(type, listener, useCapture) {
		this.form.addEventListener(type, listener.bind(this), useCapture);
	}

	dispatchFilter() {
		return this.#__customFilters.dispatchFilter(arguments);
	}

	addFilterListener(filter, fn) {
		this.#__customFilters.addFilterListener(filter, fn);
	}
}
