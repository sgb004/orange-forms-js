/**
 * @name: FFNotices
 * @author: sgb004
 * @version: 0.1.0
 */

export class FFNotices {
	static __notices = {};
	static externalNotifications = '';
	static fi = 'getExternalNotifications';

	static init() {
		this[this.fi]();
	}

	static set notices(notices) {
		this.__notices = notices;
	}

	static get notices() {
		return JSON.parse(JSON.stringify(this.__notices));
	}

	static getExternalNotifications() {
		const noticesElement = document.getElementById(this.externalNotifications);
		if (noticesElement) {
			this.notices = this.merge(noticesElement.innerHTML);
		}
		this.fi = 'none';
	}

	static none() {}

	static merge(notices) {
		notices = JSON.parse(notices);
		notices = typeof notices == 'object' && notices != null ? notices : {};
		return Object.assign(this.notices, notices);
	}
}
