# Orange Forms JS

Make form validation easy.

## Requirements

This package is thinking to use with webpack.

## Dependencies

```
custom-filters-js
lit-element
```

Check [package.json](./package.json) for more information.

## Installation

```npm
npm i orange-forms-js
```

## Usage

In your main .js add orange-forms.js

```javascript
import 'orange-forms';
```

And in your form add the class orange-form

```html
<form name="my_form" class="orange-form">...</form>
```

Your form needs a name.

The orange-forms automatically add to your form a class called OrangeForm, check the documentation below. If you require the class associated with the form you can get it using globalThis['orangeForms'], for example:

```javascript
console.log(globalThis['orangeForms']['my_form']);
```

The orange-forms.js file will also automatically import the files:

### ./css/styles.css

Loads the styles that Orange form needs.

### ./components/ff-notices-cpt.js

Loads a component related to notifications from a field or a form.
[Check the documentation for components here](./components)

### ./components/field-cpt.js

Loads a component that add a label, an input and the component ff-notices-cpt. [Check the documentation for components here](./components)

### ./orange-form

Loads the class that validate and process the data of inputs.

## Class OrangeForm

This class validates the fields that contain the field attribute of a form, obtains their values and sends them as a JSON to the url specified in the action attribute of the form. The JSON is sent via fetch.

A JSON response is expected from the url of the action attribute of the form.

### Properties

#### fields

Returns the fields that contain the field attribute in the form.

#### data

Returns the values of the fields as a JSON.

#### redirectSuccess

Returns the url where the site should go if the JSON response success is true.

#### redirectError

Returns the url where the site should go if the JSON response success is false.

#### reset

Get or set if the form will to reset when submit is complete and if the JSON response success is true.

### Filters

You can use filters in Orange Forms.

To add a filter you can use:

<!-- prettier-ignore-start -->
```javascript
globalThis['orangeForms']['my_form'].addFilterListener('filter_name', function(p1, p2, ...){
	console.log(p1, p2, ...);
	return p1;
});
```

The available filters are:

#### fields

```javascript
globalThis['orangeForms']['my_form'].addFilterListener('fields', function (fields) {
	console.log(fields);
	return fields;
});
```

#### isValid

```javascript
globalThis['orangeForms']['my_form'].addFilterListener('isValid', function (isValid, fields) {
	console.log(isvalid, fields);
	return isvalid;
});
```

#### data

```javascript
globalThis['orangeForms']['my_form'].addFilterListener('data', function (data, fields) {
	console.log(data, fields);
	return data;
});
```

#### sendOpts

You can change the fetch header options. Find "fetch header javascript" in your favorite search engine if you want more documentation.

```javascript
globalThis['orangeForms']['my_form'].addFilterListener('sendOpts', function (fetchHeader, data) {
	console.log(fetchHeader, data);
	return fetchHeader;
});
```
<!-- prettier-ignore-end -->

### Methods

#### constructor

Require a html form.

#### isValid

Returns true or false if the fields have a valid value.

#### fieldsNoticesReset

Reset field notifications.

#### send

Send the values of the fields in JSON to the url of the name attribute of the form. It expects a JSON response with the following structure:

```json
{
	"success": false,
	"notice": "There was an error, check fields.",
	"fields": {
		"field_email": [
			"Your email is not correct",
			"Your email does not a correct format."
		],
		"field_date": "The date is not correct."
	}
}
```

Where:

- **success** returns if the query was correct true or false if an error occurred.

- **notice** returns a message related to the query response.

- **fields** returns an individual message or messages related to the field.

### Events

Any event available in the html forms is available in Orange Form.

<!-- prettier-ignore-start -->
```javascript
globalThis['orangeForms']['my_form'].addEventListener('event_name', function (e) {});
```
<!-- prettier-ignore-end -->

Additionally, the following events can be used:

#### beforeValid

This event is called before checking if the fields are valid.

#### beforeSend

This event is called before sending the JSON with the values of the fields.

#### afterSend(e)

This event is called after receiving the response from the server.

In e.detail you can get the data returned by the server, check method send to know the structure of the JSON that is expected to be received.

## Notices

The file [form-notices.js](./form-notices.js) has the messages used in the form, as 'Please, check notices in fields.', if you want to translate or change these texts you can use a script tag:

```html
<script id="orange-forms-form-notices" type="application/json">
	{ "checkFields": "Please, check fields." }
</script>
```

Individually, the texts can be changed directly in the form using the notices attribute, for example:

```html
<form
	name="my_form"
	notices='{ "checkFields": "Please, check fields." }'
></form>
```

Or you can change directly using javascript:

```javascript
globalThis['orangeForms'][iterator.name].notices.checkFields =
	'Please, check fields.';
```

## Components

[You can check components documentation here.](./components)
